const express = require('express');
const app = express();
const cors = require('cors');
const axios = require('axios');

const port = process.env.PORT || 8000 ;
const { access_token } = require('./config.json');

app.use(express.json())
app.use(cors())

/*
** Get products posted on a specified date
** from Product Hunt API
*/
app.get('/post', async (req, res) => {
  const date = req.query.date;
  const url = 'https://api.producthunt.com/v2/api/graphql';
  const data = JSON.stringify({
    query: 
      `query {
        posts(postedAfter: "${date}", postedBefore: "${date}") 
        {
          edges {
            node {
              id
              name
              tagline
              description
              url
              website
              thumbnail{
                type
                url
              }
              votesCount
              reviewsRating
              topics {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          }
          pageInfo {
            hasNextPage
            startCursor
            endCursor
            hasPreviousPage
          }
        }
      }`
  });

  const opts = {
    headers: {
      "Authorization": `Bearer ${access_token}`,
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    mode: "cors"
  };

  try {
    const result = await axios.post(url, data, opts);
    if (result.data.data) {
      const products = result.data.data.posts.edges.map(el => {
        const topics = [];
        el.node.topics.edges.map(topicEl => topics.push(topicEl.node.name));
        el.node.topics = topics;
        return el.node;
      })
      res.status(200).json(products);
    }
    else {
      throw (result);
    }
  } catch(error) {
    if (error.response.status == 429) {
      res.status(429).send(error.response.headers);
    } else
      res.status(503).send(error);
  }
  
})

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});
