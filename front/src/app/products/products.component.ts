import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialog} from '@angular/material/dialog';

import { Product } from '../models/product.model';
import { Topic } from '../models/topic.model';
import { ChartModalComponent } from '../chart-modal/chart-modal.component';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Product[] = [];
  public topics: Topic[] = [];
  public date: Date = new Date();
  public dateString = '';
  public errorMsg = '';

  constructor(private productService: ProductService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getDateString();
    this.getProducts();
  }
  
  getDateString(): void {
    const d = this.date.getDate();
    const m = this.date.getMonth() + 1;
    this.dateString = (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + this.date.getFullYear();
  }
  
  getProducts(): void {
    this.getDateString();
    this.productService.getProducts(this.dateString)
    .subscribe((data: Product[]) => {
      this.products = data;
      this.getTopics();
      if (!data.length)
        this.errorMsg = `Pas de produits répertoriés ce jour`
    },
    (err) => this.errorMsg = `Error while getting product : ${err}`
    );
  }

  groupAndAdd(topicsPile: string[]): Topic[] {
     const result: Topic[] = [];
     topicsPile.forEach((el: string) => {
        let item = result.find(it => it.title == el)
        if (!item) {
          item = {title: el, count: 0}
          result.push(item)
        }
        item.count++;
     });
     return result;
  }

  getTopics(): void {
    const topicsPile: string[] = [];
    this.products.map(el => el.topics.map(top => topicsPile.push(top)));
    this.topics = this.groupAndAdd(topicsPile)
  }
  
  inputDate(event: MatDatepickerInputEvent<Date>): void {
    if (event.value) this.date = event.value;
    this.getProducts();
  }

  openDialog(): void {
    this.dialog.open(ChartModalComponent, {
      data: this.topics
    });
  }
}
