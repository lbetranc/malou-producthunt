export interface Product {
    name: string,
    description: string,
    tagline: string,
    thumbnail: {
        type: string,
        url: string
    },
    url: string,
    website: string,
    votesCount: number,
    reviewsRating: number,
    topics: string[]
}
 