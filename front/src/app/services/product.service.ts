import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Product } from '../models/product.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor ( private http: HttpClient) { }

  getProducts(paramValue: string): Observable<Product[]> {
    const params = new HttpParams().set("date", paramValue)
    return this.http.get<Product[]>(environment.apiUrl + `/post`, {params})
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.status === 0) {
      errorMessage = `An error occurred: ${error.error}`;
    } else if (error.error['x-rate-limit-reset']) {
      errorMessage = `Product hunt API limit hit : ${error.error['x-rate-limit-reset']} seconds before reset`
    } else {
      errorMessage = `Backend returned code ${error.status}, body was: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
