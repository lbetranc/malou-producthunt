import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import Chart from 'chart.js/auto';

import { Topic } from '../models/topic.model';

@Component({
  selector: 'app-chart-modal',
  templateUrl: './chart-modal.component.html',
  styleUrls: ['./chart-modal.component.css']
})
export class ChartModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: Topic[]) {}

  randColor(): string {
    return 'rgb('
      + Math.round(Math.random() * 255) + ','
      + Math.round(Math.random() * 255) + ','
      + Math.round(Math.random() * 255)
      + ')';
  }

  ngOnInit(): void {
    const labels: string[] = [];
    const data: number[] = [];
    const backgroundColor: string[] = []
    const ctx = 'myChart';

    this.modalData.map((el: Topic) => {
      labels.push(el.title);
      data.push(el.count);
      backgroundColor.push(this.randColor())
    });
    const pieData = {
      labels,
      datasets: [{
        data,
        backgroundColor
      }]
    };
    new Chart(ctx, {
      type: "pie",
      data: pieData,
    });
  }

}