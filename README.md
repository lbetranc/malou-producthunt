# Malou-producthunt

Malou-producthunt is a basic site linked to the product hunt api, developed in Angular 12 and NodeJS.

## Install

In `front/` and `back/` directories :
```
npm install
``` 

# Front

In directory `front/`

### Development server

```
npm run start
```

### Build

```
npm run build
```
The build artifacts will be stored in the `dist/` directory.
Copy everything within the output folder to a folder on the server.
Configure the server to redirect requests for missing files to index.html

# Back

In directory `back/`

Default port is 8000. Set PORT in environment to do differently
### Development server

```
npm run dev
```

### Build

```
npm run start
```

